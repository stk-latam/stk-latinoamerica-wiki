# instalacion de dependencias

**sudo apt install git build-essential cmake libbluetooth-dev libsdl2-dev libcurl4-openssl-dev libenet-dev libfreetype6-dev libharfbuzz-dev libfribidi-dev libgl1-mesa-dev libglew-dev libjpeg-dev libogg-dev libopenal-dev libpng-dev libssl-dev libvorbis-dev libxrandr-dev libx11-dev nettle-dev pkg-config zlib1g-dev subversion**

# clonacion, preparacion y compilacion

**git clone https://github.com/kimden/stk-code   
svn co https://svn.code.sf.net/p/supertuxkart/code/stk-assets stk-assets   
cd stk-code   
mkdir cmake_build && cd cmake_build   
cmake .. -DSERVER_ONLY=ON   
make -j $ ( nproc )   
sudo make install**

# creacion, modificacion e inicio del servidor

**supertuxkart --init-user --login=(usuario_del_server) --password=(contraseńa_del_usuario_del_server)   
supertuxkart --server-config=your_config.xml --network-console  luego detenemos el servicio  CTRL + C**   
**nano your_config.xml** Aquí se editan las propiedades del servidor, una vez hayas editado lo que quieras guardas el archivo con CTRL + O con el nombre original y sales con CTRL + X

**supertuxkart --server-config=your_config.xml --network-console**

# actualizacion de STK

en caso de haber nuevas versiones del codigo de STK puede actualizar haciendo lo siguiente:

En la carpeta **stk-assets** debes hacer lo siguiente:   

**svn up**   

Por último en la carpeta **stk-code** ejecute los siguientes comandos:

**git pull   
cd cmake_build   
cmake ..   
make -j $ ( nproc )   
sudo make install**

# datos a destacar

Está guía está creada para Debian y derivadas, si lo quiere pasar a otra distribución solamente cambie el apt por su gestor de paquetes o intente buscar los paquetes para su distribución en lugares como pkgs.org

para esta guia se uso el codigo de **kimden** el cual tambien es usado para **STK Latinoamérica** por su amplia funcionabilidad. **gracias a kimden por su exelente trabajo**



 

