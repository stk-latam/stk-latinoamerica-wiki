
# Nitro

las latas de nitro son impulsores de velocidad adicionales disponibles en todos los modos de juego existentes hasta el momento en STK.

![](nitro.png)

al tocar una lata de nitro ubicada en la pista recibiras una cierta cantidad de nitro que deopendera de si has tocado una lata pequeña o grande, un juador puede juntar nitro hasta que su tanque este lleno, el nivel de nitro actual se puede visualizar abajo a la derecha mientras se esta en el juego, al preionar la tecla asignada a nitro (por defecto la N) se empezará a usar el nitro que tengas acomulado hasta que sueltes la tecla o se acabe tu nitro, cuando estes usando nitro recibiras un aumento en tu velocidad que dependerá de cuanto nitro uses y de que clase de kart estes usando, algo que se recomienda es no usar todo el nitro de una, sino irlo acomulando y utilizarlo varias veces cuando sea necesario, para carreras de velocidad, en futbol y partidas en linea es muy importante usar el nitro adecuadamentee y juntar todo el nitro posible.


gracias a kimden por el contenido original