Estas reglas seran estrictamente respetadas, lealas atentamente antes de hacer cualquier cosa en los servicios de STK Latinoamérica, si quiere sugerir un cambio que no sea absurdo a las reglas puede hacerlo en nuestro [grupo de telegram](https://t.me/STKLatamchat)

# Reglas para los servidores de STK Latinoamérica

Estas reglas serán aplicadas estrictamente por lo que lea las reglas con detenimiento las veces que sean necesarias

## 1. No hay acciones inapropiadas

Dado que no es posible enumerar todos los posibles delitos menores, esta es una prohibición general de cualquier cosa que sea inapropiada. Es un asunto para los administradores decidir si algo es inapropiado.

Aquí hay algunos ejemplos de acciones inapropiadas - esto no pretende ser nada como una lista exhaustiva - así que no argumente que (lo que un administrador ha decidido es inapropiado) no está en esta lista:

 - Intentando probar los límites de los controles de los chats
 - Experimentar con comandos (o intentar ejecutarse) que están destinados solo a administradores
 - Inundar el chat ejecutando comandos aleatorios o enviando mensajes aleatorios
 - Tontear con los controles del chat
 - Trolling de cualquier tipo. Tenemos una política de tolerancia cero hacia el trolling. Si usted participa en trolling de cualquier tipo, usted será prohibido inmediatamente
 - Suplantar o intentar hacerse pasar por alguien. Si usted es descubierto, definitivamente será inmediatamente expulsado de todos nuestros grupos
 - Empujar y poner a prueba la paciencia de los administradores
 - Publicar enlaces a sitios externos sin autorizacion
 - Dar consejos/recomendaciones inapropiados a cualquier persona
 - Discutir - con cualquiera (es decir, con los administradores, o cualquier otra persona)

## 2. No piratería / malware / merchandising / promociones

 - No hable (o publique) software o malware pirateado
 - No promocionar ni promocione ningún producto, servicio, o sitio web
 - Romper cualquiera de estas sub-reglas es susceptible de atraer una prohibición automática 

## 3. Sin groserías, blasfemias, insultos / maldiciones, obscenidades, etc.

 - Cuida tu idioma y sé profesional
 - Somos una comunidad global, y las cosas que pueden ser aceptables en su país no son necesariamente aceptables en otros lugares, así que sea sensible en su elección de palabras
 - No use palabras soeces, lenguaje sexualmente explícito, palabras vulgares, expletivos u otras palabras ofensivas (en cualquier idioma). No hay absolutamente ninguna razón para jurar, maldecir o usar expletivos o blasfemias. Y - no use blasfemias, expletivos u otro lenguaje ofensivo en su nombre de usuario o perfil
 - los servidores de STK Latinoamérica utilizados por personas de todas las edades, incluidos los menores de edad, y por personas de todos los orígenes culturales. No publique enlaces o archivos con contenido pornografico de cualquier tipo
 - No seas grosero o agresivo
 - No hagas ataques personales
 - No utilice lenguaje racista/sexista, ni ningún otro lenguaje discriminatorio. No se tolerará ningún tipo de lenguaje discriminatorio
 - Romper cualquiera de estas sub-reglas es susceptible de atraer una prohibición automática de todos nuestros grupos

# reglas para los chats de STK Latinoamérica

Estas son las reglas de los chats de STK Latinoamérica. Léalos con mucho cuidado. Apegarse a la letra y al espíritu de las reglas.

 - Estas normas se aplicarán estrictamente. Por favor, tómelos muy en serio.
 - Asegúrese de familiarizarse con todas las reglas antes de publicar nada.
 - Romper una regla le dará una advertencia. Tratar de "inteligentemente" para eludir las reglas también le dará una advertencia.
 - Romper algunas reglas puede atraer una prohibición automática, potencialmente de todos nuestros grupos.
 - Si recibe 3 advertencias, esto resultará automáticamente en una prohibición.
 - El desconocimiento de las normas no será aceptado como excusa.

## 1. Las decisiones de los administradores son definitivas, y no deben ser discutidas o debatidas

 - No discuta con un administrador sobre una decisión que él / ella ha tomado; y no debatir el asunto
 - No pida a los administradores que eliminen una advertencia que le hayan emitido a usted o a cualquier otra persona
 - No participe en ninguna discusión sobre si un administrador está justificado en tomar cualquier acción que él / ella ha tomado

## 2. No hay acciones inapropiadas

Dado que no es posible enumerar todos los posibles delitos menores, esta es una prohibición general de cualquier cosa que sea inapropiada. Es un asunto para los administradores decidir si algo es inapropiado.

Aquí hay algunos ejemplos de acciones inapropiadas - esto no pretende ser nada como una lista exhaustiva - así que no argumente que (lo que un administrador ha decidido es inapropiado) no está en esta lista:

 - Intentando probar los límites de los controles de los chats
 - Experimentar con comandos (o intentar ejecutarse) que están destinados solo a administradores
 - Inundar el grupo ejecutando comandos aleatorios
 - Tontear con los controles del chat
 - Permitir que el bot antispam envíe spam al grupo
 - Trolling de cualquier tipo. Tenemos una política de tolerancia cero hacia el trolling. Si usted participa en trolling de cualquier tipo, usted será prohibido inmediatamente
 - Suplantar o intentar hacerse pasar por alguien. Si usted es descubierto, definitivamente será inmediatamente expulsado de todos nuestros grupos
 - Perdiendo el tiempo haciendo preguntas sin leer primero nuestra wiki - si haces una pregunta para la que ya se proporciona una respuesta en nuestra wiki, recibirás una advertencia
 - Perder el tiempo haciendo preguntas sin leer primero todos los mensajes anclados
 - Empujar y poner a prueba la paciencia de los administradores
 - Publicar enlaces a sitios externos sin autorizacion
 - Dar consejos/recomendaciones inapropiados a cualquier persona
 - Discutir - con cualquiera (es decir, con los administradores, o cualquier otra persona)
 - Envío de PM no solicitados a cualquier miembro de un grupo de STK Latinoamérica
 - Crear "ruido" de cualquier tipo en el grupo; esto incluye, pero no se limita a, preguntas/declaraciones sin sentido; publicar sin antes buscar en el grupo si el mismo problema ya ha sido cubierto; etc, etc

## 3. No piratería / malware / merchandising / promociones

 - No hable (o publique) software o malware pirateado
 - No promocionar ni promocione ningún producto, servicio, o sitio web
 - Romper cualquiera de estas sub-reglas es susceptible de atraer una prohibición automática

## 4. No hay etiquetado o ping de los administradores (sin autorizacion)

 - No etiquete ni haga ping a los administradores innecesariamente (ya sea directa o indirectamente). Los administradores tienen una vida fuera de telegrama, y usted debe respetar su tiempo, que están dando sin cargo
 - No importa cuán urgente pueda pensar que es su situación, debe resistir absolutamente el impulso de etiquetar o hacer ping a un administrador, porque, si etiqueta, definitivamente recibirá una advertencia
 - No pm un administrador sin primero solicitar (y asegurar) su permiso en el foro abierto
 - Si desea denunciar una publicación inapropiada, utilice el comando /report

## 5. Sin groserías, blasfemias, insultos / maldiciones, obscenidades, etc.

 - Cuida tu idioma y sé profesional
 - Somos una comunidad global, y las cosas que pueden ser aceptables en su país no son necesariamente aceptables en otros lugares, así que sea sensible en su elección de palabras
 - No use palabras soeces, lenguaje sexualmente explícito, palabras vulgares, expletivos u otras palabras ofensivas (en cualquier idioma). No hay absolutamente ninguna razón para jurar, maldecir o usar expletivos o blasfemias. Y - no use blasfemias, expletivos u otro lenguaje ofensivo en su nombre de usuario o perfil
 - los chats de STK Latinoamérica utilizados por personas de todas las edades, incluidos los menores de edad, y por personas de todos los orígenes culturales. No publique imágenes desnudas o pornográficas de ningún tipo (incluida la exposición de genitales masculinos o femeninos y/o senos femeninos)
 - No seas grosero o agresivo
 - No hagas ataques personales
 - No utilice lenguaje racista/sexista, ni ningún otro lenguaje discriminatorio. No se tolerará ningún tipo de lenguaje discriminatorio
 - Romper cualquiera de estas sub-reglas es susceptible de atraer una prohibición automática de todos nuestros grupos
