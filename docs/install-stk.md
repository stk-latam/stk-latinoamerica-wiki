# descargas

todas las formas oficiales de descargar SuperTuxKart

## Flathub

SuperTuxKart Flathub package   
build date: 2020-10-07   
[download](https://flathub.org/apps/details/net.supertuxkart.SuperTuxKart)   

## Github

SuperTuxKart github repos

 - [STK-Leatest Preview Release](https://github.com/supertuxkart/stk-code/releases/preview)
 - [STK-Code](https://github.com/supertuxkart/stk-code)
 - [STK-Stats](https://github.com/supertuxkart/stk-stats)
 - [STK-Editor](https://github.com/supertuxkart/stk-editor)
 - [STK-Blender](https://github.com/supertuxkart/stk-blender)
 - [STK-Dependencies](https://github.com/supertuxkart/dependencies)
 - [STK-Assets - Mobile](https://github.com/supertuxkart/stk-assets-mobile)
 - [STK-Addons](https://github.com/supertuxkart/stk-addons)
 - [STK-Code(Kimden)](https://github.com/kimden/stk-code/)

## packages

SuperTuxKart 1.2   
build date: 2020-08-28   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/1.2/)   

SuperTuxKart 1.2 RC1   
build date: 2020-07-21   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/1.2-rc1/)   

SuperTuxKart 1.1   
build date: 2020-01-04   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/1.1/)   

SuperTuxKart 1.0   
build date: 2019-04-20   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/1.0/)   

SuperTuxKart 0.9.2   
build date: 2017-11-26    
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.9.2/)   

SuperTuxKart 0.9.3   
build date: 2017-11-23   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.9.3/)   

SuperTuxKart 0.9.1   
build date: 2016-02-02   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.9.1/)   

SuperTuxKart 0.9   
build date: 2015-04-28   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.9/)   

SuperTuxKart 0.8.1   
build date: 2014-02-18   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.8.1/)   

SuperTuxKart 0.8   
build date 2013-07-03   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.8/)   

SuperTuxKart 0.2   
build date: 2013-02-15   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.2/)   

SuperTuxKart 0.7.3   
build date: 2012-10-31   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.7.3/)   

SuperTuxKart 0.7.2   
build date: 2011-08-24   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.7.2/)   

SuperTuxKart 0.7.1   
build date: 2011-04-29   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.7.1/)   

SuperTuxKart 0.7   
build date: 2011-01-11   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.7/)   

SuperTuxKart 0.6.2   
build date: 2010-11-22   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.6.2/)   

SuperTuxKart 0.5   
build date: 2010-09-01   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.5/)   

SuperTuxKart 0.6.1a   
build date: 2009-02-19   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.6.1a/)   

SuperTuxKart 0.6.1   
build date: 2009-02-17   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.6.1/)   

SuperTuxKart 0.6   
build date: 2009-01-25   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.6/)   

SuperTuxKart 0.4   
build date: 2008-03-27   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.4/)    

SuperTuxKart 0.3   
build date: 2007-08-06   
[download](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.3/)   

# instalación

Primero elija la versión que quiera, su sistema y su arquitectura

## Windows
 
 - una vez descargada la versión que quiera y el archivo para su arquitectura haga click derecho sobre el archivo, luego haga click en ejecutar como administrador.

 - una vez el instalador esté abierto toque en siguiente, acepte los términos para poder continuar y de siguiente. Seleccione la ubicación del juego (recomiendo la predeterminada). De en siguiente. Luego se le preguntará si quiere accesos directos al juego pero eso es a elección. Luego de en instalar

 - (en el caso de que no tenga visual C++ se abrirá una ventana donde solo debe dar en instalar y luego en cerrar)

 - por último de en terminar 

 - A DISFRUTAR

## Linux

 - Debian/Ubuntu y derivadas: sudo apt install supertuxkart

 - Fedora: sudo dnf/yum install supertuxkart

 - Arch:: pacman/yay -S supertuxkart

 - Flatpak (universal) sudo flatpak install supertuxkart 

 - En caso de no aparecer deben escribir sudo flatpak install https://flathub.org/apps/details/net.supertuxkart.SuperTuxKart

 - **Hay más gestores pero estos son de los principales.**

 - Si nada funciona descargar de #descargas el paquete para linux, extraer el archivo y en la ruta donde encuentren un .ah deben abrir un terminal y hacer lo siguiente:

 - chmod +x run_game.sh

 - Luego pueden simplemente darle doble click o en el terminal hacer

 - ./run_game.sh 

 - O bash run_game.sh

 - A DISFRUTAR

## Mac

 - Descargar el archivo

 - Extraer el .zip

 - Entrar a la carpeta

 - Ejecutar el archivo

 - DISFRUTAR

## Android 

 - Puede buscar supertuxkart en la tienda que use para su teléfono 

 - En caso de no usar puede hacer lo siguiente 
 
 - elija la versión que desea y el link lo llevará ala pagina de descargas de esa versión dónde debes elegir el apk para la arquitectura de su soc.

 - Una vez descargado puede abrir el apk y darle a instalar.

 - A DISFRUTAR