# Addons

En el equipo de STK Latinoamérica hicimos packs de addons para que no tengas que descargar todos los addons 1 a 1, estos son nuestros tipos de packs.

## Normal

este tipo de packs traen la carpeta addons para que extraigas en tu carpeta de STK y queden instalados directamente.

## Core

esto solo incluye las carpetas de addons para poder ser instaladas desde el servidor con este comando 

**/installaddon (enlace al pack (mas abajo))**

**no habran combinaciones como tracks+arenas**

# descargas

## normal

Karts   
tamaño del archivo: 163.67 MB   
[descargar aqui](https://file2linktsbot553.herokuapp.com/106532)

karts + arenas   
tamaño del archivo: 374.5 MB    
[decargar aqui](https://file2linktsbot553.herokuapp.com/106533)

karts + tracks   
tamaño del archivo: parte 1: 633.67 MB parte 2: 1.5 GB  
[descargar aqui (parte 1)](https://file2linktsbot553.herokuapp.com/106530) [descargar aqui (parte 2)](https://file2linktsbot553.herokuapp.com/106526)

tracks   
tamaño del archivo: 1.94 GB   
[descargar aqui](https://file2linktsbot553.herokuapp.com/106540)

tracks + arenas   
tamaño del archivo: parte 1: 680.64 MB parte 2: 1.5 GB   
[descargar aqui (parte 1)](https://file2linktsbot553.herokuapp.com/106538) [descargar aqui (parte 2)](https://file2linktsbot553.herokuapp.com/106536)

arenas   
tamaño del archivo: 212.27 MB   
[descargar aqui](https://file2linktsbot553.herokuapp.com/106528)

full   
tamaño del archivo: parte 1: 845.86 MB parte 2: 1.5 GB   
[descargar aqui (parte 1)](https://file2linktsbot553.herokuapp.com/106524) [descargar aqui(parte 2)](https://file2linktsbot553.herokuapp.com/106522)

## core

karts: https://file2linktsbot553.herokuapp.com/106544   
tracks: https://file2linktsbot553.herokuapp.com/106540   
arenas: https://file2linktsbot553.herokuapp.com/106542   

# guia de instalación **(packs normales)**

##**Windows:**

Extraes el .zip (si el archivo tiene 2 partes debes descargar y extraer los 2) y pasas la carpeta "addons" a:
C:\users/(usuario)/.Appdata/roaming/supertuxkart/

##**Linux:**

Extraes el .zip (si el archivo tiene 2 partes debes descargar y extraer los 2) y pasa la carpeta "addons" a :
~/.local/share/supertuxkart/

##**Mac:**

próximamente

##**Android:**

Extraes el zip (si el archivo tiene 2 partes debes descargar y extraer los 2) y pasa la carpeta "addons" a:
/storage/emulated/0/Android/data/org.supertuxkart.stk/files/supertuxkart/home/.local/share/supertuxkart

# contacto

si un enlace se cae le pedimos por favor que nos contacte a nuestro [grupo de telegram](https://t.me/STKLatamchat)

