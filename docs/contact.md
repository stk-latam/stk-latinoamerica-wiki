
# Contacto

si necesita o quiere contactarse con nosotros lo puede hacer de 3 formas diferentes:

 - telegram: [Click Aqui](https://t.me/STKLatamchat)
 - discord: [Click Aqui](https://discord.gg/gYKzucu6by)
 - matrix: [Click Aqui](#STKLatam:matrix.org)

estas tres formas de comunicacion estan conectadas entre si para llegar a mas gente, esto se hizo con la ayuda de matterbridge [Click aqui para mas informacion](https://github.com/42wim/matterbridge), muchas gracias a 42wim por su trabajo.
