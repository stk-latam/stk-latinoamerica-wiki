# Texturas Comprimidas

texturas optimizadas para un mayor rendimiento en computadoras de gama baja

## Informaion 

texturas de SuperTuxKart 1.2 comprimidas para un mayor rendimiento, las texturas se mantienen exactamente iguales a las originales pero comprimidas y empaquetadas para un mayor rendimiento.

## Descarga

puede descargar las texturas haciendo [click aqui](https://drive.google.com/file/d/1n5C7ny4cLhg_mdtwwfO1Z0Ulb7dANnTa/view?usp=sharing)

## Instalacion

dentro de la carpeta data de su instalacion de STK elimine la carpeta textures y remplacela con la carpeta extraida del zip de arriba